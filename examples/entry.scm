(use-modules (newt bindings)
             (newt components)
             (newt checkboxtree)
             (newt entry)
             (newt form)
             (newt grid)
             (newt listbox)
             (newt scale)
             (newt textbox)
             (newt window)
             (srfi srfi-1))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")
(make-window 10 5 40 25 "Entry Sample")

(define e1 (make-entry 1 1 30 #:initial-value "Sample content"))
(define b1 (make-button 1 20 "Ok"))
(define f1 (make-form))

(add-component-to-form f1 e1)
(add-component-to-form f1 b1)

(set-entry-text e1 "Sample content edited" #t)
(entry-set-cursor-position e1 3)
(entry-set-filter e1 (lambda (entry ch cursor)
                       ;; Do not allow the 'E' letter.
                       (if (= ch 69)
                           0
                           ch)))

(run-form f1)

(newt-finish)
