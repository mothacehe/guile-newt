(use-modules (newt bindings)
             (newt components)
             (newt checkboxtree)
             (newt form)
             (newt listbox)
             (newt window))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")
(make-window 10 5 40 25 "Checkboxtree Sample")

(define cbt1 (make-checkboxtree 1 1 10 (logior FLAG-SCROLL FLAG-BORDER)))
(define f1 (make-form))

(define key1 (add-entry-to-checkboxtree cbt1 "ITEM 1" 0))
(define key2 (add-entry-to-checkboxtree cbt1 "ITEM 2" 0))
(define key3 (add-entry-to-checkboxtree cbt1 "ITEM 3" 0))

(define b1 (make-button 1 20 "Ok"))

(add-components-to-form f1 cbt1 b1)

(set-current-checkboxtree-entry cbt1 key2)
(set-checkboxtree-entry-text cbt1 key2 "ITEM2 EDITED")
(set-checkboxtree-width cbt1 20)
(set-checkboxtree-entry-value cbt1 key2 #\*)

(run-form f1)

(newt-finish)
