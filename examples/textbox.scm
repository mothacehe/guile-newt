(use-modules (newt bindings)
             (newt components)
             (newt checkboxtree)
             (newt form)
             (newt listbox)
             (newt scale)
             (newt textbox)
             (newt window))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")
(make-window 10 5 40 20 "Sample Textbox")

(define tb1
  (make-reflowed-textbox
   1 1
   "[[https://www.gnu.org/software/guix/][GNU Guix]] (IPA: /ɡiːks/) is
a purely functional package manager, and associated free software
distribution, for the [[https://www.gnu.org/gnu/gnu.html][GNU
system]].  In addition to standard package management features, Guix
supports transactional upgrades and roll-backs, unprivileged package
management, per-user profiles, and garbage collection."
   35 #:flags FLAG-BORDER))

(define f1 (make-form))

(define b1 (make-button 1 15 "Ok"))

(add-component-to-form f1 tb1)
(add-component-to-form f1 b1)

(run-form f1)

(newt-finish)
