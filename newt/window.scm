;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt window)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:export (clear-screen
            resize-screen
            wait-for-key
            clear-key-buffer
            make-window
            make-centered-window
            pop-window
            pop-window-no-refresh
            push-help-line
            redraw-help-line
            pop-help-line
            draw-root-text
            bell-terminal
            disable-cursor
            enable-cursor
            screen-size

            message-window
            choice-window
            ternary-window))

(define (clear-screen)
  "Clear the screen content."
  (let ((proc (libnewt->procedure void "newtCls" '())))
    (proc)))

(define (resize-screen redraw?)
  "Resize the screen and redraw it if REDRAW? is #t."
  (let ((proc (libnewt->procedure void "newtResizeScreen" `(,int))))
    (proc (if redraw? 1 0))))

(define (wait-for-key)
  "Return when the user has pressed a key."
  (let ((proc (libnewt->procedure void "newtWaitForKey" '())))
    (proc)))

(define (clear-key-buffer)
  "Clear the content of the terminal input buffer without waiting for
additional input."
  (let ((proc (libnewt->procedure void "newtClearKeyBuffer" '())))
    (proc)))

(define (make-window left top width height title)
  "Create a window at (LEFT, TOP) coordinates with the given WIDTH,
HEIGTH and TITLE."
  (let ((proc (libnewt->procedure int
                                  "newtOpenWindow"
                                  `(,int ,int ,unsigned-int ,unsigned-int *))))
    (proc left top width height (string->pointer title))))

(define (make-centered-window width height title)
  "Create a centered window with the given WIDTH, HEIGHT and TITLE."
  (let ((proc (libnewt->procedure int
                                  "newtCenteredWindow"
                                  `(,unsigned-int ,unsigned-int *))))
    (proc width height (string->pointer title))))

(define (pop-window)
  "Remove the top window from the display and redraw the display areas
which the window overwrote."
  (let ((proc (libnewt->procedure void "newtPopWindow" '())))
    (proc)))

(define (pop-window-no-refresh)
  "Remove the top window from the display without redrawing the
display areas which the window overwrote."
  (let ((proc (libnewt->procedure void "newtPopWindowNoRefresh" '())))
    (proc)))

;; TODO: newtSetColors

;; TODO: newtSetColor

(define (push-help-line help-line)
  "Push the given HELP-LINE string to the help line."
  (let ((proc (libnewt->procedure void "newtPushHelpLine" '(*))))
    (proc (string->pointer help-line))))

(define (redraw-help-line)
  "Force the redrawing of the help line."
  (let ((proc (libnewt->procedure void "newtRedrawHelpLine" '())))
    (proc)))

(define (pop-help-line)
  "Pop the content of the help line."
  (let ((proc (libnewt->procedure void "newtPopHelpLine" '())))
    (proc)))

(define (draw-root-text column row text)
  "Write the given text on the root window at (COLUMN, ROW)
coordinates."
  (let ((proc (libnewt->procedure int
                                  "newtDrawRootText"
                                  `(,int ,int *))))
    (proc column row (string->pointer text))))

(define (bell-terminal)
  "Send a beep to the terminal."
  (let ((proc (libnewt->procedure void "newtBell" '())))
    (proc)))

(define (disable-cursor)
  "Disable the cursor."
  (let ((proc (libnewt->procedure void "newtCursorOff" '())))
    (proc)))

(define (enable-cursor)
  "Enable the cursor."
  (let ((proc (libnewt->procedure void "newtCursorOn" '())))
    (proc)))

(define (screen-size)
  "Return the screen size as two values, the columns count and the
rows count."
  (let ((proc (libnewt->procedure void
                                  "newtGetScreenSize"
                                  '(* *))))
    (let ((columns (make-c-struct (list int) (list 0)))
          (rows (make-c-struct (list int) (list 0))))
      (proc columns rows)
      (values (car (parse-c-struct columns (list int)))
              (car (parse-c-struct rows (list int)))))))

(define (message-window title button-text text)
  "Create a message window with the given TITLE. The TEXT string will
be displayed and a simple button with BUTTON-TEXT will be created
above."
  (let ((proc (libnewt->procedure int
                                  "newtWinMessage"
                                  '(* * *))))
    (proc (string->pointer title)
          (string->pointer button-text)
          (string->pointer text))))

(define (choice-window title button1-text button2-text text)
  "Create a window with the given TITLE. The TEXT string will be
displayed and two buttons with BUTTON1-TEXT and BUTTON2-TEXT will be
created above."
  (let ((proc (libnewt->procedure int
                                  "newtWinChoice"
                                  '(* * * *))))
    (proc (string->pointer title)
          (string->pointer button1-text)
          (string->pointer button2-text)
          (string->pointer text))))

(define (ternary-window title button1-text button2-text button3-text text)
  "Create a window with the given TITLE. The TEXT string will be
displayed and three buttons with BUTTON1-TEXT, BUTTON2-TEXT and
BUTTON3-TEXT will be created above."
  (let ((proc (libnewt->procedure int
                                  "newtWinTernary"
                                  '(* * * * *))))
    (proc (string->pointer title)
          (string->pointer button1-text)
          (string->pointer button2-text)
          (string->pointer button3-text)
          (string->pointer text))))
