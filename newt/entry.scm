;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt entry)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-entry
            set-entry-text
            entry-value
            set-entry-flags
            entry-cursor-position
            entry-set-cursor-position
            entry-set-filter))

(define* (make-entry left top width
                     #:key (initial-value "") (flags 0))
  "Create a text entry at (LEFT, TOP) coordinates, with the given
INITIAL-VALUE. The WIDTH and the FLAGS applying to this entry have to
be specified."
  (let ((proc (libnewt->procedure '*
                                  "newtEntry"
                                  `(,int ,int * ,int * ,int))))
    (pointer->entry (proc left top
                          (string->pointer initial-value)
                          width %null-pointer flags))))

(define* (set-entry-text entry text
                         #:key
                         (cursor-at-end? #t))
  "Set the TEXT of the given ENTRY. The boolean CURSOR-AT-END?
indicates whether or not the cursor has to be moved to the end of the
TEXT."
  (let ((proc (libnewt->procedure void
                                  "newtEntrySet"
                                  `(* * ,int))))
    (proc (entry->pointer entry) (string->pointer text)
          (if cursor-at-end? 1 0))))

(define (entry-value entry)
  "Return the value of the given ENTRY."
  (let ((proc (libnewt->procedure '*
                                  "newtEntryGetValue"
                                  '(*))))
    (pointer->string (proc (entry->pointer entry)))))

(define (set-entry-flags entry flags flag-role)
  "Set the FLAGS applying to given ENTRY. The FLAG-ROLE (set, reset or
toggle) has to be specified."
  (let ((proc (libnewt->procedure void
                                  "newtEntrySetFlags"
                                  `(* ,int ,int))))
    (proc (entry->pointer entry) flags flag-role)))

(define (entry-cursor-position entry)
  "Return the position of the cursor in the given ENTRY as an integer."
  (let ((proc (libnewt->procedure int
                                  "newtEntryGetCursorPosition"
                                  '(*))))
    (proc (entry->pointer entry))))

(define (entry-set-cursor-position entry position)
  "Set the cursor to POSITION for the given ENTRY."
  (let ((proc (libnewt->procedure void
                                  "newtEntrySetCursorPosition"
                                  `(* ,int))))
    (proc (entry->pointer entry) position)))

(define entry-filter-cb
  (lambda (callback)
    (procedure->pointer
     int
     ;; This callback will be called with the ENTRY that received an
     ;; input. CHARACTER is the ascii code of the character received
     ;; and CURSOR the current position of the CURSOR in the entry.
     (lambda (entry data character cursor) ; DATA is %null-pointer for now.
       (callback entry character cursor))
    `(* * ,int ,int))))

(define (entry-set-filter entry filter-callback)
  "Set FILTER-CALLBACK as the procedure to be called for each new
input character for the given ENTRY."
  (let ((proc (libnewt->procedure void
                                  "newtEntrySetFilter"
                                  `(* * *))))
    (proc (entry->pointer entry)
          (entry-filter-cb filter-callback)
          %null-pointer)))
