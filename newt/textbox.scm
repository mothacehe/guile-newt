;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt textbox)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-textbox
            set-textbox-text
            set-textbox-height
            textbox-lines

            make-reflowed-textbox
            reflow-text))

(define (make-textbox left top width height flags)
  "Create a textbox at (LEFT, TOP) coordinates with the given WIDTH,
HEIGHT and FLAGS."
  (let ((proc (libnewt->procedure '*
                                  "newtTextbox"
                                  `(,int ,int ,int ,int ,int))))
    (pointer->textbox (proc left top width height flags))))

(define (set-textbox-text textbox text)
  "Set the content of the given TEXTBOX to text."
  (let ((proc (libnewt->procedure void
                                  "newtTextboxSetText"
                                  '(* *))))
    (proc (textbox->pointer textbox) (string->pointer text))))

(define (set-textbox-height textbox height)
  "Set the height of the given TEXTBOX to HEIGHT."
  (let ((proc (libnewt->procedure void
                                  "newtTextboxSetHeight"
                                  `(* ,int))))
    (proc (textbox->pointer textbox) height)))

(define (textbox-lines textbox)
  "Return the number of text lines in the given TEXTBOX."
  (let ((proc (libnewt->procedure int
                                  "newtTextboxGetNumLines"
                                  '(*))))
    (proc (textbox->pointer textbox))))

(define* (make-reflowed-textbox left top text width
                                #:key
                                (flex-down 0)
                                (flex-up 0)
                                (flags 0))
  "Create a textbox at (LEFT, TOP) coordinates. The given TEXT will be
inserted in the textbox. Newt will reflow the text so that it looks
rectangular for the given WIDTH. The longest line will be comprised
between WIDTH + FLEX-UP and WIDTH - FLEX-DOWN. The FLAGS passed will
also be honored."
  (let ((proc (libnewt->procedure '*
                                  "newtTextboxReflowed"
                                  `(,int ,int * ,int ,int ,int ,int))))
    (pointer->textbox (proc left top
                            (string->pointer text)
                            width flex-down flex-up flags))))

(define (reflow-text text width flex-down flex-up)
  "Return the given TEXT reflowed using the WIDTH, FLEX-DOWN and
FLEX-UP parameters. This is the mechanism used in
'make-reflowed-textbox'."
  (let ((proc (libnewt->procedure '*
                                  "newtReflowText"
                                  `(* ,int ,int ,int * *))))
    (let* ((actual-width (make-c-struct (list int) (list 0)))
           (actual-height (make-c-struct (list int) (list 0)))
           (result (proc (string->pointer text)
                         width flex-down flex-up
                         actual-width actual-height)))
      (values (parse-c-struct actual-width (list int))
              (parse-c-struct actual-height (list int))
              (pointer->string result)))))
