;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt scale)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-scale
            set-scale-value))

(define (make-scale left top width full-value)
  "Create a progress bar at (LEFT, TOP) coordinates with the given
WIDTH. FULL-VALUE is an integer corresponding to 100% of the scale."
  (let ((proc (libnewt->procedure '*
                                  "newtScale"
                                  `(,int ,int ,int ,unsigned-long))))
    (pointer->scale (proc left top width full-value))))

(define (set-scale-value scale value)
  "Set the progress of the given SCALE to VALUE. VALUE must be
comprised between 0 and the full scale value. Newt will compute the
corresponding percentage for the scale."
  (let ((proc (libnewt->procedure void
                                  "newtScaleSet"
                                  `(* ,unsigned-long))))
    (proc (scale->pointer scale) value)))
